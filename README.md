Proof that I can code and hopefully useful snippets for folks.
===

Gruntfile: Soon to be my all encompassing test suite for automating sanity checking of code samples
---

js/binary-search.js
---
A node application that exhibits a passable attempt at a binary search tree. The input files it was designed against are a little funky, but you can see them in 
inputs/binary-search/

To run it
   node binary-search.js ../input/input000.txt (The size should match the corresponding output).

js/json-call-backs-in-jquery.js
---
Some old ajax stuff I kept around. Soon to be rewritten, but still pretty good.

py/reshare_chaining.py
---
Some lovely python gutting from an unobtrusive (or proprietary)

java/android.py
---
Facebook authentication on android.
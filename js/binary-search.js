var BINARY_TREE = (function () {
    'use strict';
    function BinaryTree() {
        this.nodes = {};
        this.level = 0;
        this.node = 0;

        this.stats = { lefts: 1, rights: 0, depth: 1};
        //Do some fancy math to determine the coordinates of a given binary tree node
        //Odd indexes to the left, even indexes to the right
        //node = 0; level = 0
        //0 + (2^0) - 1
        //0 + 1  - 1 = 0 first index)

        //To move the left, we go
        //level = parent level + 1 = 1
        //node = parent node * 2 =  0
        //node = 0; level = 1
        //0 + (2^1) - 1
        //0 + 2 - 1 = 1 second index (first child to the left)


        //to move to the right, we go
        //level = parent level + 1 = 1
        //node = parent node * 2 + 1
        //node = 1; level 1 (but to the right)
        //1 + (2^1) - 1 = 2
        this.locator = function (level, node) {
            var location = node + Math.pow(2, level) - 1;
            return location;
        };

        this.setNode = function (value, level, node) {
            //Set a node to the current map
            if (level === undefined) {
                this.nodes[this.locator(this.level, this.node)] = value;
            } else {
            //set the specified node to the specified value
                this.nodes[this.locator(level, node)] = value;
            }
        };

        this.getNode = function (level, node) {
            //return the current map if no coordinates given
            if (level === undefined) {
                return this.nodes[this.locator(this.level, this.node)];
            }
            //return the specified node
            return this.nodes[this.locator(level, node)];
        };

        this.root = function (value) {
            this.setLevel(0);
            this.node = 0;
            //Instantiate the root node if it does not exist
            if (value !== undefined) {
                this.nodes[this.locator(this.level, this.node)] = value;
            }
            return this.nodes[this.locator(this.level, this.node)];
        };

        this.leftChild = function (value) {
            this.setLevel(this.level + 1); //a child's level is n + 1
            this.node = this.node  * 2; //a left childs node is parent node * 2
            if (value !== undefined) {
                console.log('Adding left child value ' + value + ' to level: ' + this.level + ' node ' + this.node);
                this.nodes[this.locator(this.level, this.node)] = value;

            }
            return this.nodes[this.locator(this.level, this.node)];
        };

        this.setLevel = function (value) {
            this.level = value;
            if (this.level > this.stats.depth) {
                this.stats.depth = this.level;
            }
        };

        this.rightChild = function (value) {
            this.setLevel(this.level + 1); //a child's level is n + 1
            this.node = this.node * 2 + 1;  //a riight child node is left node + 1 (parent node * 2 + 1)
            if (value !== undefined) {
                this.nodes[this.locator(this.level, this.node)] = value;
            }
            return this.nodes[this.locator(this.level, this.node)];
        };

        this.hasLeftChild = function () {
            //Peek ahead to make tranversing sane
            var peek_level, peek_node;
            peek_level = this.level + 1;
            peek_node = this.node * 2;
            //return true if the child node exists
            return (this.nodes[this.locator(peek_level, peek_node)] !== undefined);
        };

        this.hasRightChild = function () {
            //Peek ahead to make tranversing sane
            var peek_level, peek_node;
            peek_level = this.level + 1;
            peek_node = this.node * 2 + 1;
            return (this.nodes[this.locator(peek_level, peek_node)] !== undefined);
        };


        this.parent = function (value) {
            this.setLevel(this.level - 1);
            this.node = (this.node >> 1);
            if (value !== undefined) {
                this.nodes[this.locator(this.level, this.node)] = value;
            }
            return this.node[this.locator(this.level, this.node)];
        };

        this.length = function () {
            //Since we're using an associative "array" to store locations
            //nodes.length will always be zero.
            //Loop over object properties insteard
            var size = 0, location;
            for (location in this.nodes) {
                if (this.nodes.hasOwnProperty(location)) {
                    size += 1;
                }
            }
            return size;
        };

        //Walk the tree and see what's up
        this.traverse = function () {

            //be very careful here. These a recursive and they move the pointer around the map
            console.log('Traversing:' + this.getNode());
            console.log(this.level);
            if (this.level > this.stats.depth) {
                this.stats.depth = this.level;
            }
            if (this.hasLeftChild()) {
                console.log('Moved left to ' + this.leftChild());
                this.stats.lefts += 1;
                this.traverse();
            }
            if (this.hasRightChild()) {
                console.log('Moved right to ' + this.rightChild());
                this.stats.rights += 1;
                this.traverse();
            }
            if (this.level > 0) {
                this.parent();
                console.log('No child left behind. Moved back to ' + this.getNode());
            }
        };
    }
    return BinaryTree;
}());

var BINARY_TREE_RUNNER = (function (module, BinaryTree) {
    'use strict';
    module.input = "";
    module.load = function () {
        process.stdin.resume();
        process.stdin.setEncoding("ascii");
        module.input = "";
        process.stdin.on("data", function (chunk) {
            module.input += chunk;
        });

        process.stdin.on("end", function () {
            var split_input, tree, direction = 'left';
            split_input = module.input.split('\n');
            console.log(split_input);
            //recursive function
            //takes an array of strings and a binary tree object
            function parse_tree(split_input, tree) {
                var item, term_signals;
                item = split_input.shift().trim();
                term_signals = 0;
                //The file formats deliminate the binary tree in this way
                //case 1:
                //n: 4
                //n: 3 (no term signal)
                //case 2:
                //4 is a parent of 3 and 3 is on the left branch
                // n: 4
                // TERM
                // n: 3 (one term signal)
                //4 is a parent of 3 and 3 is on the right branch
                //case 3:
                //n:4
                //TERM
                //TERM
                //n: 3
                //4 is NOT a parent of 3 and 3 belongs on the right branch of the next node
                //if 2 * n TERM signals are encountered, we move up n nodes
                while (item === "TERM") {
                    term_signals += 1;
                    direction = 'right';
                    if (term_signals > 1) {
                        tree.parent();
                    }
                    item = split_input.shift().trim();
                }
                //All right, now we know what direction and the internal cursor is on the right parent node
                //Add our item to the left or right branch
                if ((item.length > 0) && (item % 1 === 0)) {
                    console.log('Parsing: ' + item + " Direction: " + direction);
                    if (tree.length() === 0) {
                        //First node!
                        console.log("setting root");
                        tree.root(item);
                        console.log(tree);
                    } else if (direction === 'left') {
                        //Add children down the left side of the tree
                        console.log("adding left child");
                        tree.leftChild(item);
                    } else if (direction === 'right') {
                        console.log("adding right child");
                        tree.rightChild(item);
                        //go back to adding to the left side of the just added right child
                        direction = 'left';
                    }
                }
                if (split_input.length > 0) {
                    tree = parse_tree(split_input, tree);
                }
                return tree;
            }
            tree = parse_tree(split_input, new BinaryTree());
            tree.root();
            tree.traverse();
            console.log(tree.nodes);
            console.log(tree.stats);
            console.log('Size: ' + (tree.stats.lefts + tree.stats.depth));
        });

    };

    module.load();
}({}, BINARY_TREE));

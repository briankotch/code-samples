function page_data() {
    'use strict';
    var xhr;
    this.save_user_info = function (settings, success_function, error_function) {
        xhr = $.ajax({
            type: "POST",
            url: "/api/update_user_info",
            data: settings,
            dataType: "json",
            success: function (data) {
                if (data.error) {
                    error_function(data.error);
                } else {
                    success_function();
                }
            }
        });
        return xhr;
    };

    this.update_interest_categories = function (settings, success_function, error_function) {
        xhr = $.ajax({
            type: "POST",
            url: "/api/set_interest_categories",
            data: settings,
            dataType: "json",
            success: function (data) {
                if (data.error) {
                    error_function(data.error);
                } else {
                    success_function();
                }
            }
        });
        return xhr;
    };
}

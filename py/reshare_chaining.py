def get_full_reshare_chain(video_ids):
    """Get all the reshares, meaning the ones stemming from the
    original video id, for a list of video_ids. Returns a map.

    """
    if not video_ids:
        return {}

    video_ids = [get_int(video_id) for video_id in video_ids.split(',')]

    user_map = {}
    format_string = ','.join(['%s' for x in video_ids])
    query = "SELECT users.user_id, users.user_name, users.email, users.about, users.picture, users.name, videos.video_id AS video_id, videos.original_video_id, videos.reshared_video_id, videos.source_video_id, videos.num_likes, videos.num_comments, videos.comment FROM videos LEFT JOIN users USING(user_id) WHERE videos.video_id IN(" + format_string + ") OR videos.original_video_id IN(" + format_string + ")"
    rows = db.exec_read_query(query, video_ids * 2)
    if not rows:
        return {}

    video_ids = frozenset(video_ids)
    video_map = dict(((row['video_id'], row) for row in rows if row['video_id'] in video_ids))

    original_videos = []
    for row in rows:
        if row.get('original_video_id'):
            original_videos.append((row['original_video_id'], row['source_video_id']))
        else:
            original_videos.append((row['video_id'], row['source_video_id']))
    original_videos = frozenset(original_videos)

    format_string = ','.join(['%s' for x in original_videos])
    query = "SELECT users.user_id, users.user_name, users.email, users.about, users.picture, users.name, videos.video_id AS video_id, videos.original_video_id, videos.reshared_video_id, videos.num_likes, videos.num_comments, videos.comment FROM videos LEFT JOIN users ON users.user_id = videos.user_id WHERE (videos.original_video_id IN(" + format_string + ") OR (videos.original_video_id IS NULL AND videos.video_id IN(" + format_string + "))) AND videos.source_video_id IN(" + format_string + ") ORDER BY videos.video_id"
    vals = zip(*original_videos)[0] * 2 + zip(*original_videos)[1]
    reshared_videos = db.exec_read_query(query, vals) or []

    for video in reshared_videos:
        if not video.get('video_id') or not video.get('user_id'):
            continue
        fixup_pic_urls(video)
        if not video.get('original_video_id'):
            video['original_video_id'] = video['video_id']
        for video_id in video_ids:
            if video_map[video_id]['original_video_id'] == video['video_id']:
                if not video_map[video_id].get('reshares'):
                    video_map[video_id]['reshares'] = []
                video_map[video_id]['reshares'].append(video)
                video_map[video_id]['original_sharer'] = video
            elif video_map[video_id]['original_video_id'] == video['original_video_id'] or video['original_video_id'] == video_id:
                if not video_map[video_id].get('reshares'):
                    video_map[video_id]['reshares'] = []
                video_map[video_id]['reshares'].append(video)
    for k in video_map.iterkeys():
        video_map[k]['num_shares'] = len(video_map[k]['reshares'])
    return video_map

//A sample from an Android version of a social video platform I worked on
     
public class Login_Task extends AsyncTask<Void, Void, JSONObject>  {
     
    private Context m_context;
    private Results_Listener m_listener;
     
    public Login_Task(Context _context) {
        m_context = _context;
    }
           
           
    public void set_listener(Results_Listener listener) {
        m_listener = listener;
    }
     
    @Override
        protected void onPreExecute() {
        super.onPreExecute();
    }
           
    private JSONObject login_facebook(String access_token, long access_expires) {
        ParameterMap parameters = new ParameterMap();
        HttpResponse response = null;
        Facebook facebook = new Facebook(m_context.getString(R.string.fb_prod_app_id));
        facebook.setAccessToken(access_token);
        facebook.setAccessExpires(access_expires);
        if(facebook.isSessionValid()) {
            parameters.add("access_token", access_token);
            try {
                response  = Api_Requester.post(m_context, Constants.URL + "api/login_facebook",
                                               parameters);
                JSONObject result = new JSONObject(response.getBodyAsString());
                return result;
            } catch (NoHttpResponseException e) {
                parameters.add("url",  "api/login_facebook");
                Bugsnag.notify(e, parameters);
            } catch (JSONException e) {
                parameters.add("url",  "api/login_facebook");
                Bugsnag.notify(e, parameters);
            } catch (Exception e) {
                parameters.add("url",  "api/login_facebook");
                Bugsnag.notify(e, parameters);
            }
        }
        return null;
    }
     
    private JSONObject login(String username, String password) {
        ParameterMap parameters = new ParameterMap();
        HttpResponse response = null;
        parameters.add("username", username);
        parameters.add("password", password);
        parameters.add("ajax", "true");
        try {
            response =  Api_Requester.post(m_context, Constants.URL + "api/login_for_ios", parameters);
            JSONObject result = new JSONObject(response.getBodyAsString());
            return result;
        } catch (NoHttpResponseException e) {
            parameters.add("url",  "api/login");
            Bugsnag.notify(e, parameters);
        } catch (JSONException e) {
            parameters.add("url",  "api/login");
            Bugsnag.notify(e, parameters);
        } catch (Exception e) {
            parameters.add("url",  "api/login");
            Bugsnag.notify(e, parameters);
        }
        return null;
    }
           
           
    @Override
        protected JSONObject doInBackground(Void... params) {
        Data_Cache cache = new Data_Cache(m_context);
        JSONObject result = null;
        int retries = 0;
        while(retries < 3) {
            if (cache.get_access_token().length() > 0) {
                result = login_facebook(cache.get_access_token(), cache.get_access_expires());
            } else if (cache.get_username().length() > 0) {
                result = login(cache.get_username(), cache.get_password());
            } else {
                Api_Requester.logout(m_context);
                break;
            }
            if(result == null) {
                retries++;
            } else {
                break;
            }
        }
        return result;
    }
     
    @Override
        protected void onPostExecute(JSONObject result) {
        super.onPostExecute(result);
        Boolean success = false;
        if(result != null) {
            success = result.optBoolean(JSON_KEYS.SUCCESS, false);
        }
        if(success) {
            m_listener.on_results_succeeded(result);
        } else {
            m_listener.on_results_failed(result.optString(JSON_KEYS.ERROR, "Error signing in.");
                                         }
                   
                   
        }
           
    }

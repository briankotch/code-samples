'use strict';
module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            code: { 
                files: [
                    'js/*.js',
                ],
                tasks: ['test'],
                options: {
                }
            },
        },
        jslint: {
            client: {
                src: 'js/*.js', 
                directives: { 
                    browser: true,
                    bitwise: true,
                    predef: ['jQuery', '$', 'console', 'process']
                }
            },
        },
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-jekyll');
    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-copy-to');
    grunt.registerTask('test', ['jslint']);
    grunt.registerTask('build', ['test']);
    grunt.registerTask('default', ['build']);
};
